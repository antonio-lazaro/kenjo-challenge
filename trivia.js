/*
 *	Trivia game
 */
const argsParser = require('args-parser');
const fetch = require('node-fetch');
const readlineSync = require('readline-sync');
const fs = require('fs');

const BASE_API_URL = 'https://opentdb.com/api.php';
const SCORES_FILE = 'scores.txt';

var args = argsParser(process.argv);

if (!args.o) {
	console.log('Param -o not found')
	return;
}

/**
 * Returns the questions from the API
 * @function fetchQuestions
 * @param nQuestions - Number of questions
 * @param dificulty - Easy, medium or hard
 * @returns 
 */
async function fetchQuestions(nQuestions, difficulty) {
	let url = `${BASE_API_URL}?amount=${nQuestions}&difficulty=${difficulty}`;
	let data = await fetch(url);
	let questions = (await data.json()).results;
	if (await !questions) {
		console.log('API request error')
		process.exit()
	};
	return await questions;
}

/**
 * Returns a random integer in the specified range
 * @function getRandomInt
 * @param min - Min number (inclusive)
 * @param max - Max number (exclusive)
 */
function getRandomInt(min, max) {  
	return Math.floor(Math.random() * (max - min) + min)
}

/**
 * Loads the questions and starts teh game
 * @function playQuiz
 */
function playQuiz() {
	// Load the questions from the API
	console.log('Loading questions...')
	let nQuestions = parseInt(args.nQuestions) || 4;
	let difficulty = parseInt(args.difficulty) || 'easy';
	fetchQuestions(nQuestions, difficulty)
		.then(questions => {
			// Iterate over questions
			console.log('Quiz starts!\n')
			let nCorrect = 0; // Number of correct answers
			questions.forEach(question => {
				// Display question and options
				let questionText = `${question.question}\n`;
				let correctOptionPos = getRandomInt(0, question.incorrect_answers.length + 1); // Calculate correct answer position randomly
				let incorrectIndex = 0; // Index of the next incorect option to take
				for (let i = 0; i < (question.incorrect_answers.length + 1); i += 1) {
					let answer;
					if (i == correctOptionPos) {
						answer = question.correct_answer;
					} else {
						answer = question.incorrect_answers[incorrectIndex];
						incorrectIndex += 1;
					}
					questionText += `${i}: ${answer}\n`;
				}

				console.log(questionText);
				let optionPos = parseInt(readlineSync.keyIn(`Answer (0-${question.incorrect_answers.length}): `));
				// Validate input
				while (optionPos < 0 || optionPos > question.incorrect_answers.length) {
					console.log('Wrong input');
					optionPos = readlineSync.keyIn(`Answer (0-${question.incorrect_answers.length}): `);
				}
				if (optionPos == correctOptionPos) {
					nCorrect += 1;
					console.log('Correct!\n')
				} else {
					console.log('Incorrect\n')
				}
			})

			// Calculate final score
			let finalScore = nCorrect / questions.length;
			console.log(`Score: ${finalScore * 100}% (${nCorrect}/${questions.length})`)

			// Save score
			let scoreRow = `${(new Date()).toISOString()} ${finalScore * 100}%\n`;
			fs.appendFile(SCORES_FILE, scoreRow, function (err) {
				if (err) return console.log(err);
			});
		});
}

/**
 * List the scores
 * @function listScores
 */
function listScores() {
	console.log('Scores:')
	fs.readFile(SCORES_FILE, 'utf8', function (err,data) {
		if (err) return console.log(err);
		console.log(data);
	});
}

switch (args.o) {
	case 'p':
		playQuiz()
		break;
	case 's':
		listScores()
		break;
	default:
		console.log('Invalid -o value (it should be p, to play, or s, to list the scores)')
		process.exit()
}