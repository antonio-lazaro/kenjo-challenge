# Trivia Game

## Description

Trivia console game developed with Node JS `v12.17.0`.

## Packages

Three packages have been used to make the code more simple and easier to read.

- `args-parser` to parse the input arguments/params in an easy way.
- `node-fetch` to make the requests to the API easier.
- `readline-sync` to read the user's input quickly and synchronously.

## Installation

1. Clone repository from Bitbucket:
    `$ git clone https://antonio-lazaro@bitbucket.org/antonio-lazaro/kenjo-challenge.git`

1. Install dependencies:
    `$ npm install`

## Run

To run it:
`$ node trivia.js [params]`

The params have the next form:
`$ [paramName]=[paramValue]`

### Parameters

#### Mandatory

The `-o`param is mandatory. It can have two values (`p`).

|option|description|
| --------- | ------- |
|`p`|Play a new game|
|`s`|List the scores of previous games|

Example:
`$ node trivia.js -o=p`

#### Optional

The `p` option has two extra optional params:

|name|description|default|
| --------- | ------- | ------- |
|`nQuestions`|Number of questions|`4`|
|`difficulty`|Game difficulty (`easy`, `medium` or `hard`)|`easy`|

Example:
`$ node trivia.js -o=p -nQuestions=2 -difficulty=easy`

![imgs/run-example.png](imgs/run-example.png)

